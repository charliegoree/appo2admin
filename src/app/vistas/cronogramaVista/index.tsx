import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { useState, useContext} from 'react';


import style from './style.scss';
import { LoginContext } from './../../contexto/index';
import Menu from '../../componentes/menu';
import ItemLista from '../../componentes/itemLista';
import Modal from '../../componentes/modal';
import { Clase } from './../../datos';
import EditarClase from '../../componentes/formularios/editarClase';
import useDatabase from '../../hooks/useDatabase';



export default function CronogramaVista(props:any){
    const login = useContext(LoginContext);
    const [togle,setTogle] = useState<boolean>(false);
    const [clase,setClase] = useState<any>({});
    
    function editar(clase: Clase){
        setClase(clase);
        setTogle(true);
    }
    function mostrarEditor(){
        if (togle){
            return <EditarClase clase={clase} />;
        } else {
            return <div></div>
        }
    }
    if (login.logueado == false){
        return <Redirect to="/" />
    } else {
        return (
            <React.Fragment>
                <Menu />
                <div className={style.cuerpo}>
                    <div className={style.contenedor}>
                        <div className={style.fila}>
                            <h2>Lista Clases</h2>
                        </div>
                        <div className={style.lista}>
                            {
                                login.clases.map((clase,key)=>{
                                    return <ItemLista item={{nombre:clase.nombre, cupo: clase.cupo+' alumnos'}} key={key} editar={(e)=>editar(clase)} />
                                })
                            }
                        </div>
                    </div>
                </div>
                <Modal togle={togle} setTogle={setTogle}>
                    {
                        mostrarEditor()
                    }
                </Modal>
            </React.Fragment>
        )
    }
}
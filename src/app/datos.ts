/*
export interface Cuota {
    id?: string;
    nombre: string;
    duracion: number;
    valor: number;
}
export interface Cliente {
    id?: string;
    nombre: string;
    telefono: string;
    cuota: string;
    fecha: Date;
    anotado: string;
    delta:number;
}
export interface Clase {
    id?:string;
    nombre:string;
    cupo:number;
}
export interface Horario{
    id?: string;
    dia: number;
    hora: string;
    clases: Array<string>;
    cupo:number;
    clientes: Array<string>;
}
*/

export interface Cliente {
    id?: string;
    nombre: string;
    apellido: string;
    telefono: string;
    cuota: string;
    dayCuota: number;
    monthCuota: number;
    yearCuota: number;
    dayTurno: number;
    monthTurno: number;
    yearTurno: number;
    horaTurno: number;
    claseTurno: string;
}
export interface Cuota {
    id?: string;
    nombre: string;
    valor: number;
    cantidad: number;
}
export interface Clase {
    id?:string;
    nombre: string;
    cupo: number;
}
export interface Turno {
    cliente: string;
    clase: string;
    dia: string;
    dayTurno: number;
    monthTurno: number;
    yearTurno: number;
    horaTurno: number;
}

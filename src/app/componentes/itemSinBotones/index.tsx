import * as React from 'react';
import { useState, useContext} from 'react';


import style from './style.scss';


export default function ItemSinBotones(props:any){
    return (
        <div className={style.item}>
            {
                Object.values(props.item).map((dato:any,key:number)=>{
                    return <div className={style.texto} key={key}>{dato}</div>
                })
            }
        </div>
    )
}
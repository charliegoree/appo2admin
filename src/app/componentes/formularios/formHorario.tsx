import * as React from 'react';
import { useState, useEffect} from 'react';


import style from './style.scss';


export default function FormHorario(props:any){
    
    const [dia,setDia] = useState<number>(0);
    const dias = [1,2,3,4,5,6,7];
    const diasNombres = ['','lunes','martes','miercoles','jueves','viernes','sabado','domingo'];

    function cerrar(e){
        e.preventDefault();
        props.cerrar();
    }
    function editar(e){
        e.preventDefault();
        props.cerrar();
    }
    function mostrarDia(){
        if (dia != 0){
            return <Editor dia={dia} cerrar={setDia} />
        } else {
            return <div></div>
        }
    }
    return (
        <div className={style.formulario}>
            <h2>Cambiar Cupo maximo del dia</h2>
            <div className={style.fila}>
                <select value={dia} onChange={(e)=> setDia(parseInt(e.currentTarget.value))}>
                    <option value={0}>Elegir un dia</option>
                    {
                        dias.map((d,key)=>{
                            return <option key={key} value={d}>{diasNombres[d]}</option>
                        })
                    }
                </select>
            </div>
            <div className={style.fila}>
            {
                mostrarDia()
            }
            </div>
            <div className={style.botonera}>
                <button onClick={cerrar}>Cerrar</button>
            </div>
        </div>
    )
}

function Editor(props:any){
    const {horarios,cambiarCupoHorarios} = useHorarios(props.dia);
    const [cupo,setCupo] = useState<string>('0');
    const [primera,setPrimera] = useState<boolean>(true);
    useEffect(()=>{
        if (primera && horarios.length > 0){
            setCupo(horarios[0].cupo.toString());
            setPrimera(false);
        }
    })
    function editar(e){
        e.preventDefault();
        cambiarCupoHorarios(parseInt(cupo));
        props.cerrar(0);
    }
    
    
    return (
        <React.Fragment>
            <div className={style.fila}>
                <p>Cupo Maximo: </p><input type="text" value={cupo} onChange={(e)=>setCupo(e.currentTarget.value)} />
            </div>
            <div className={style.botonera}>
                <button onClick={editar}>Cambiar</button>
            </div>
        </React.Fragment>
    )
}